# Welcome to the ROpenExo documentation website
![Vue globale de l'exosquelette](./docs/mecanic/mecanic-RO4-overview.png)

## Find the 3d models
You can find the 3d models just [here](./docs/.vuepress/public/3d), we are working actually to make them more visible and easier to download.

## Website settings
This website is developping by using [vuepress](https://vuepress.vuejs.org/) tool which is based on the progressive framework [VueJs](https://vuejs.org/)

### Setup git and clone
* Get [git](https://git-scm.com/downloads) if you don't have it
* Setup the ssh key in your gitlab ()
```bash
git clone git@gitlab.com:udez/ropenexo.git
```
### Install
* Get [NPM](https://npmjs.com/) if you don't have it
* Install the nodes modules
```bash
npm install
```

### Run on developpment
```bash
npm run docs:dev
```

### Build the documentation
```bash
npm run doc:build
```
Then get files from `./public` folder

### Need to fix
[ ] All the 3d are [here](./docs/.vuepress/public/3d), we need to make them more visible