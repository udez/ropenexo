let sidebarTest = [
    '/',
    '/mecanic/',
    '/about/'

];
var path = require('path');
console.log("path.resolve(__dirname, '../../designs')", path.resolve(__dirname, '../../designs'));
module.exports = {
    title: 'ROpenExo',
    description: 'L\'exosquelette membre supérieur pour toutes et tous !',
    base: process.env.GITLAB_CI ? '/ropenexo/' : '/',
    dest: 'public',
    themeConfig: {
        // displayAllHeaders: false,
        sidebar: 'auto',
        sidebarDepth: 3,
        nav: [
            { text: 'Accueil', link: '/' },
            { text: 'Version mecanique ROE4', link: '/mecanic/' },
            { text: 'A propos', link: '/about/' },
            // { text: 'Contribuer', link: '/about/contribute' },
        ],
        sidebar: sidebarTest
    },
    extendMarkdown: md => {
        md.set({ breaks: true })
        md.use(require('markdown-it-video', {
            youtube: { width: 640, height: 390 },
            vimeo: { width: 640, height: 390 },
            vine: { width: 640, height: 390, embed: 'simple' },
            prezi: { width: 640, height: 390 }
        }))
    },
    head: [
        ['link', { rel: 'icon', href: '/logo-dessn.jpg' }],
        ['link', { rel: 'manifest', href: '/manifest.json' }],
        ['meta', { name: 'theme-color', content: '#3eaf7c' }],
        ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
        ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
        ['link', { rel: 'apple-touch-icon', href: '/logo-152x152.png' }],
        ['link', { rel: 'mask-icon', href: '/logo-152x152.png', color: '#3eaf7c' }],
        ['meta', { name: 'msapplication-TileImage', content: '/logo-144x144.png' }],
        ['meta', { name: 'msapplication-TileColor', content: '#000000' }]
    ],
    
    plugins: {
        '@vuepress/pwa': {
            serviceWorker: true,
            updatePopup: true
        },
        '@vuepress/search': {
            searchMaxSuggestions: 10
        },
        'sitemap': {
            hostname: 'https://udez.gitlab.io/ropenexo/'
        },
        '@vuepress/back-to-top':{}
          // '@vuepress/blog': {
        //     postsDir: '/assignments/'
        // }
    }
}