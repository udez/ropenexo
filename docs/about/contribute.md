---
home: false
title: Contribuer 
---

# Comment je peux contribuer
Si le projet vous intéresse, n'hésitez pas à contribuer, quelques que soit vos compétences, nous pourrions avoir besoin de vous.

## Participation à la co-création de l'exosquelette
A ce stade d'avancement, nous n'avons pas encore de solution fonctionnel, convaincu par la force de l'intelligence collective, nous avons décidé de construire l'éxosquelette dans le cadre d'une démarche living lab.

### Rejoignez-nous durant les sprints lab au carrefour numérique²
Retrouvez nous, durant les sprints lab pour participer aux ateliers de co-construction de ROpenExo.

### Vous n'êtes pas sur Paris, mais vous souhaitez quand même co-construire avec nous ? 
N'hésitez pas à nous contacter, nous serions ravis d'échanger avec vous.

## Documentation
### Recherche pour définir l'état de l'art des solutions
Nous sommes constamment à la recherches des solutions déjà existantes, nous en avons repéré un certains nombre que nous listons sur cette page, mais nous sommes cosnscient qu'il est fort probable que nous soyons passé à côté de nombreuses solutions, et nous comptons sur vous pour nous aider à voir plus largement.

### Vérification et correction de la documentation
Parceque nous sommes pas des machines, nous avons besoin d'aide sur la vérification, correction et amélioration de notre documentation.

### Traduction de la documentation
Afin de rendre la technologie accessible au plus grand nombre, nous sommes à la recherche de traducteurs (anglais, espagnole, ...).

## Communication
N'hésitez pas à nous suivre et à relayer nos différentes communications afin de nous aider à acquérir davantage de visibilité afin de faire rayonner et prospérer le projet.