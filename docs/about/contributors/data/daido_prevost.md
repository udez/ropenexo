---
home: false
layout: ContributorLayout
title: Daido Prevost
image: /contributors/daido-prevost.jpg
summary: Le Perceval de la table ronde de la cour du RopenExo a plusieurs cordes à son arc qu'il met pleinement à disposition de l'équipe et du projet.
---
Expert en procrastination, un peu flemmard, désintéressé par la plupart des sujets, peu cultivé pour certain, aucun diplôme encore en poche, très peu d’expérience dans l’informatique ; la modélisation, la robotique et tout ce qui concerne les nouvelles technologies.

Mais malgré tout ça il a deux grande qualités:

1. La première c’est qu’il est pas très intelligent donc dès qu’il s’intéresse à un projet ou un sujet il fonce tête baissée même s’il y connait rien.
2. Et la deuxième c’est sa « Percevalite aigüe » c’est le fait de toujours faire l’effort d’apprendre, de comprendre et de faire son maximum le sujet dont on lui parle ou le projet sur lequel il travail malgré son manque d’expérience et de connaissance.

Véritable ignorant intelligent, il a une grande culture musicale et a beaucoup lu et regardé de mangas.