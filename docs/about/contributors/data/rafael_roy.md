---
home: false
layout: ContributorLayout
title: Rafaël Roy
image: /contributors/rafael-roy.jpg
summary: Initiateur de l'idée, ayant beaucoup d'imagination et déjà identifier nombre de besoins, Rafaël est le membre essentiel de l'équipe et du projet.
---
Rafael est au collège Henri Bergson dans le 19e arrondissement de Paris. Il est passionné par les jeux vidéo et les nouvelles technologies. Il participe au projet RopenExo car c’est l’un des fondateurs de celui-ci. Il participe à ce projet aussi pour apprendre tout un tas de choses.

Grand blagueur Rafaël a toujours une ou deux blagues sous le manteau pour détendre l'atmosphère et il adore les soupes aux lardons :joy: