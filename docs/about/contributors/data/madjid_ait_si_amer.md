---
home: false
layout: ContributorLayout
title: Madjid Ait Si Amer
image: /contributors/madjid-ait_si_amer.jpg
summary: Madjid a démarré le projet à partir de l'idée de Rafaël, avec diverses compétences, il porte le projet au travers de sa société Udez.
---
Passionné depuis sa jeunesse de nouvelles technologies et l'entreprenariat, Madjid se définit comme un "ignorant intelligent", car assoiffé de connaissances, il aime s'intéresser à divers domaines, apprendre et tester pour trouver des solutions aux problèmes. Ayant un double diplôme en informatique (MIAGE) et en management des systèmes d'informations, il a écrit un mémoire sur innovation ouverte, qui a marqué son état d'esprit. Il a poursuivi son parcours en entreprenant, et en travaillant au sein de startups en tant que développeur full stack. Sa soif de connaissances et d'exploration technologique, l'a amené à s'intéresser tout d'abord à l'impression 3d, puis à se former au sein de la [FabAcademy](http://fabacademy.org/2019/labs/sorbonne/students/ait-siamermadjid/).

Ayant connu Rafaël depuis sa naissance, un lien quasi fraternel s'est tissé entre les deux.  C'est ainsi, qu'après une évolution importante de la maladie de Rafaël, il s'est mis à la recherche de solutions pour répondre aux besoins de Rafaël.

Le premier prototype qu'il a fabriqué, lui fera réaliser toute la complexité sous-jacente à la réalisation d'un exosquelette ergonomique. Dans le but d'être accompagné dans la démarche de co-construction avec l'utilisateur, il a intégrer la résidence [Muséocamp 2020](http://www.cite-sciences.fr/fr/au-programme/lieux-ressources/carrefour-numerique2/living-lab/residences-museocamp-2020/), et s'est mis à la recherche d'une super équipe pluridisciplinaire afin de co-créer avec Rafaël, une solution d'aide technique qui puissent être accessible au plus grand nombre.

Ancien danseur de breakdance, il a fait du parkour en club avant de prendre sa retraite, pour devenir un professionnel, expert du [Catane :game_die:](https://fr.wikipedia.org/wiki/Les_Colons_de_Catane). Il ne sait faire qu'un dessert, mais qu'il maitrise à la perfection, le fondant au chocolat :chocolate_bar: !