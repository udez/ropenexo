---
home: false
layout: ContributorLayout
title: Antoine Lenoir
image: /contributors/antoine-lenoir.jpg
summary: Etudiant en Biotechnologie, bidouilleur fou, Antoine n'hésite pas à mettre son humour au service de la bonne humeur.
---
Avec un père dans la cybersécurité et un frère gamer, Antoine tomba très jeune dans la marmite de l'informatique.

La lecture d'œuvres de science fiction faisait germer dans l'esprit du petit homme des rêves aussi fous qu'improbables. Pourtant, alors qu'il était en quête d'une école post bac pour l'accueillir, Antoine tomba nez à nez sur l'E-Smart Lab, le FabLab d'une école de l'ESME, son actuelle école. C'est ainsi que naquit l'amour de l'étudiant pour le bidouillage, ou plutôt le making (ça fait plus classe).

C'est justement lors d'un Hackaton au FabLab de la Villette qu'il rencontra le grand et l'unique Madjid, qui lui parla de ROpenExo. A la seule entente du mot "exosquelette", Antoine se surprenait déjà à rêver. Il en informa également la magnifique Clotilde, une amie qu'il savait intéressée par le projet.

Ah oui, Attention, il est fière de sa moustache. On touche pas à la moustache, la moustache c'est sacré... :man: