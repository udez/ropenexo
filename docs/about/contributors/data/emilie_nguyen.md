---
home: false
layout: ContributorLayout
title: Emilie Nguyen
image: /contributors/emilie-nguyen.jpg
summary: Ingénieure en innovation, designer de produir de produits et de services, Emilie dispose d'une énergie débordante qu'elle met pleinement sur le projet.
---
Emilie est ingénieure en innovation et designer de produits et de services, spécialisée dans le domaine de la santé. Elle rencontre Madjid au FabLab de la Cité des Sciences alors qu'elle travaillait sur un concours pour des personnes en situation de handicap. La première version d'exosquelette imprimée par Madjid titille la curiosité d'Emilie, et pleine d'idées d'amélioration en tête, elle lui propose immédiatement de travailler sur le projet!

Multi-casquettes, Emilie s'investit particulièrement sur la partie créative : son regard de designer lui permet d'animer des ateliers de groupe pour faire émerger de nouvelles pistes et solutions innovantes. Elle participe également à la conception, modélisation 3D, la veille et la mise en relation.

Et sinon, Emilie a une énergie débordante qui peut en effrayer plus d'un à la première rencontre!
