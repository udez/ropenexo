---
home: false
layout: ContributorLayout
title: Clotilde Barbot
image: /contributors/clotilde-barbot.jpg
summary: Etudiante en ingénierie, Clotilde est déterminée à contribuer au projet pour le meilleur et pour le pire.  
---
Clotilde est étudiante à l’ESME Sudria, une école d’ingénieur généraliste. Elle a rencontré Madjid par Antoine qui l’avait lui-même rencontré lors d’un hackathon (c’est fou). Etant amis, Antoine sait qu’elle est passionnée de robotique et veut être ingénieur dans l’aide médicale, et lui parle donc du projet. Avant même d’avoir plus de détail, Clotilde accepte tout de suite d’y participer.

Ce qu’elle adore dans ce projet, c’est la véritable aide concrète que cela va apporter à Rafaël, et l’opportunité de travailler avec des personnes plus expérimentées et ayant des profiles différents du sien.

Par contre, Clotilde déteste les moustiques et est nulle en cuisine, mais elle adore la série Friends !