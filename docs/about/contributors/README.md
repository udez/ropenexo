---
home: false
title: Contributeurs
---
<!-- <div class="contributors">
    <div class="contributor">
        <div class="contributor-img">
            <img src="/android-chrome-512x512.png" class="" alt="Photos d'Emilie" />
        </div>
        <div class="contributor-content">
            <p>Emilie est ingénieure en innovation et designer de produits et de services, spécialisée dans le domaine de la santé. Elle rencontre Madjid au FabLab de la Cité des Sciences alors qu'elle travaillait sur un concours pour des personnes en situation de handicap. La première version d'exosquelette imprimée par Madjid titille la curiosité d'Emilie, et pleine d'idées d'amélioration en tête, elle lui propose immédiatement de travailler sur le projet!</p>
            <p>Multi-casquettes, Emilie s'investit particulièrement sur la partie créative : son regard de designer lui permet d'animer des ateliers de groupe pour faire émerger de nouvelles pistes et solutions innovantes. Elle participe également à la conception, modélisation 3D, la veille et la mise en relation.</p>
            <p>Et sinon, Emilie a une énergie débordante qui peut en effrayer plus d'un à la première rencontre!</p>
        </div>
    </div>
</div> -->
<Contributor page="contributors/data" />
