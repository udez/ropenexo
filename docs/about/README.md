---
home: false
title: A propos
---

# {{$page.title}}

## **Pourquoi ROpenExo ?**

Pour aider Rafael, un proche atteint de la myopathie de Duchenne, a retrouver un peu d'autonomie (notamment pour manger)   

**L'origine du Nom**

**R** pour Rafaël, l'instigateur du projet.

**Open** pour Open source, le modèle de développement choisit pour le projet.

**Exo** pour Exosquelette 

Le projet ROpenExo consiste donc à créer une solution open source d'exosquelette de bras fonctionnel, ergonomique et accessible économiquement pour des personnes ayant des déficiences motrices du bras.

## Comment contribuer à ce site

Ce site est hébergé sur [gitlab](https://gitlab.com/udez/ropenexo), vous trouverez la documentation pour initialiser le projet juste [ici](../website/init.md)

## Contact
[contact@ropenexo.com](mailto:contact@ropenexo.com)