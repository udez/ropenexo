---
home: false
title: Comment contribuer au site internet ?
---

## Pré-requis
### IDE - Environnement de développement intégrée
Si vous êtes familier avec le développement, vous savez sans doute ce qu'est un IDE, et sans doute que vous avez déjà votre préférence, et ne vous invitons à enrichir la documentation avec vos logiciels favoris.

Néanmoins, pour les débutants, et même les plus expérimentés, d'utiliser [VSCode](https://code.visualstudio.com/), un IDE ergonomique, gratuit et opensource. Il gère nativement une console intégré, des utilitaires git (fichiers modifié etc...)

### Git
#### Installation
_Pour le projet, nous avons choisi d'utiliser [gitlab](https://gitlab.com/) pour gérer le dépôt [git](https://fr.wikipedia.org/wiki/Git) du projet._
_:warning: si vous voulez, contribuer et publier vos modifications, vous devez créer un compte sur [gitlab](https://gitlab.com/users/sign_up)_
1. Télécharger et installer [Git](https://git-scm.com/downloads)
2. Dans un terminal exécuter les commandes suivantes
```bash
git config --global user.name "YOUR_USERNAME"
git config --global user.email "your_email_address@example.com"
git config --global core.autocrlf false #to prevent problem of wsl systems when using ubuntu and windows system
```
3. Copier la clé ssh sur gitlab
Ouvrir Git-GUI sans forcément viser un dépôt local.
`Menu Aide` > `Bouton Montrer la clef SSH` > `Bouton Générer une clef`
Protégez votre clef privée avec une passphrase (phrase de passe ou phrase secrète, _optionnel_).
> les fichiers contenant ces clefs se situent dans votre répertoire personnel (~ avec Git-Bash), dans .ssh.

Reste à enregistrer cette clef publique (copier-coller) dans [gitlab](https://gitlab.com/profile/keys).

4. Cloner le site internet de ROpenExo sur votre ordinateur, taper la commande suivante dans le terminal de commande (dans le dossier où vous souhaitez placer le projet).
`git clone git@gitlab.com:udez/ropenexo.git`


## Développement du site en local
Ouvrez le dossier ropenexo dans VSCode, et ouvrez un terminal dedans (`Terminal` > `New terminal`, ou `Ctrl`+`Shift`+`ù`)

### Installer les dépendences
* Installer une version [LTS de nodejs](https://nodejs.org/en/download/) si vous n'avez pas déjà node d'installé.
* Installer les dépendences du projet en lançant la commande suivant dans votre terminal
```bash
npm install
```

### Exécuter le site en local
Lancer la commande suivante et aller voir le résultat à l'adresse suivante [localhost:8080](http://localhost:8080/).

```bash
npm run docs:dev
```

### Le markdown
Le [markdown](https://fr.wikipedia.org/wiki/Markdown) est principalement utilisé pour faciliter la saisie de la documentation sur le site internet. Vous pouvez trouver sur cette [adresse](https://udez.gitlab.io/doc-mkdocs/markdown/) une page expliquant les bases de la syntaxe markdown.

Nous vous conseillons aussi fortement, d'aller voir la documentation officiel de [vuepress](https://vuepress.vuejs.org/guide/markdown.html).


### Git
#### Process
1. Récupération des dernières versions (`git pull`)
2. Création d'une nouvelle branche (`git checkout -b <nom_de_la_nouvelle_branche>` ou utiliser les commandes visuels de vscode)
![screenshot_git_vscode](../assets/img/git_vscode_newbranch.jpg)
3. Chaque modification est ajouté dans des commits locals (`git commit -m "<votre_message_de_modif>"`ou utiliser les commandes visuels de vscode)
![screenshot_git_commit](../assets/img/git_vscode_commit.jpg)
4. Envoyer la branche sur le serveur (`git push`)

#### Emojis
La liste des émojis est [ici](https://www.webfx.com/tools/emoji-cheat-sheet/).
Par exemple pour avoir un :smile: il vous suffit d'ecrire `:smile:` dans le fichier markdown que vous modifiez. 
### Commandes git

- git [clone (copier)](https://git-scm.com/docs/git-clone/fr) 
Clone un dépôt dans un nouveau répertoire

```bash
 git clone git@gitlab.com:udez/ropenexo.git
```

- git [checkout](https://git-scm.com/docs/git-checkout/fr)
`git checkout -b <nom_branche>` : Création et basculement vers une nouvelle branche
`git checkout <nom_branche>` : Basculer vers la branche `<nom_branche>`

```bash
$> git branch
master
ma_branche
feature_inprogress_branch
$> git checkout ma_branche
```

- git [commit](https://git-scm.com/docs/git-commit)
Annoter les modifications faites, permet de décrire les différentes modifications qui ont été faites (`git log`, pour voir l'historique).

```bash
git commit –m “Description du commit”
```

- git [push](https://git-scm.com/docs/git-push)
Envoyer les modifications fait sur le repo gitlab du projet.

```bash
 git push origin master
 git push 
```

- git [pull](https://git-scm.com/docs/git-pull)
Récupérer les modifications faites sur le repo gitlab du projet.

```bash
git checkout master
git pull origin master
```


## Tutoriels
* [vuepress](https://vuepress.vuejs.org/)
@[youtube](https://www.youtube.com/watch?v=89ugTesiVHc)
