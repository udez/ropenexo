---
home: true
actionText: Découvrir la version mécanique →
actionLink: ./mecanic/
features:
- title: Pour les utilisateurs
  details: Solution pensé dans une démarche UX  
- title: une solution ouverte
  details: Sous la licence creative commons
- title: et accessible
  details: ROpenExo se veut accessible pour un grand nombre de personnes 
footer: CC BY SA - 2019-present - Udez    
---
