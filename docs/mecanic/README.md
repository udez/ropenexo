---
home: false
title: Exosquelette mécanique ROE4
---

# Exosquelette mécanique 
![Vue globale de l'exosquelette](./mecanic-RO4-overview.png)


## Ensemble des pièces 
[[toc]]

### Vue globale
<View3D src="/3d/mecanic-0-4/ROE4-210104-assemblage-1.STL"/>

### Arc épaule (x1)
<View3D src="/3d/mecanic-0-4/ROE4-210104-shoulder blade segment-1.STL"/>
 
### Articulation épaule (x2)
<View3D src="/3d/mecanic-0-4/ROE4-210104-shoulder joint-1.STL"/>

### Articulation coude (x1)
<View3D src="/3d/mecanic-0-4/ROE4-210104-elbow joint-1.STL"/>

### Articulation poignet (x1)
<View3D src="/3d/mecanic-0-4/ROE4-210104-wrist joint-4.STL"/>

### Clip articulation poignet (x1)
<View3D src="/3d/mecanic-0-4/ROE4-210104-wrist joint clip-1.STL"/>

### Support coude (x1)
<View3D src="/3d/mecanic-0-4/ROE4-210104-elbow holder-2.STL"/>

### Support poignet (x1)
<View3D src="/3d/mecanic-0-4/ROE4-210104-wrist holder-2.STL"/>

### Barre coude (x1)
<View3D src="/3d/mecanic-0-4/ROE4-210104-elbow rod-1.STL"/>

### Support poignet en H (x1)
<View3D src="/3d/mecanic-0-4/ROE4-210104-wrist H support-2.STL"/>

### Support elastique S (x3)
<View3D src="/3d/mecanic-0-4/ROE4-210104-elastic holder-2.STL"/>

### Support elastique M (x1)
<View3D src="/3d/mecanic-0-4/ROE4-210104-large elastic holder-2.STL"/>


### Barre Sup (x3)
<View3D src="/3d/mecanic-0-4/ROE4-210104-rod sup-1.STL"/>


## Notice de montage
:warning: TODO